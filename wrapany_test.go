// Copyright (c) 2017 Charles Francoise.  All rights reserved.
//
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package wrapany

import (
	"encoding/json"
	"reflect"
	"sync"
	"testing"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/golang/protobuf/ptypes/wrappers"
)

func testWrap(t *testing.T, in interface{}, expect proto.Message) *any.Any {
	t.Helper()

	t.Logf("wrapping %v", in)
	out, err := Wrap(in)
	if err != nil {
		t.Fatalf("error wrapping %v: %s", in, err)
	}

	messageName, _ := ptypes.AnyMessageName(out)
	t.Logf("%T wrapped to %v", in, messageName)
	if !ptypes.Is(out, expect) {
		t.Fatalf("type mismatch wrapping %v: expected %s, got %s", in, proto.MessageName(expect), messageName)
	}

	m, _ := proto.Marshal(expect)
	if !reflect.DeepEqual(out.GetValue(), m) {
		t.Fatalf("failed wrapping %v: expected %v, got %v", in, m, out.GetValue())
	}

	return out
}

func testUnwrap(t *testing.T, in interface{}, expect interface{}) interface{} {
	t.Helper()

	m, _ := Wrap(in)

	t.Logf("unwrapping %v", m)
	out, err := Unwrap(m)
	if err != nil {
		t.Fatalf("error unwrapping %v: %s", m, err)
	}

	messageName, _ := ptypes.AnyMessageName(m)
	t.Logf("%v unwrapped to %T", messageName, out)
	if !reflect.DeepEqual(expect, out) {
		t.Fatalf("failed unwrapping %v: expected %v, got %v", m, expect, out)
	}

	return out
}

//
// Wrapping tests
//

func TestWrapNil(t *testing.T) {
	testWrap(t, nil, &empty.Empty{})

	var s []byte
	testWrap(t, s, &wrappers.BytesValue{Value: nil})
}

func TestWrapBool(t *testing.T) {
	testWrap(t, true, &wrappers.BoolValue{Value: true})
	testWrap(t, false, &wrappers.BoolValue{Value: false})
}

func TestWrapInt32(t *testing.T) {
	testWrap(t, int8(42), &wrappers.Int32Value{Value: 42})
	testWrap(t, int16(42), &wrappers.Int32Value{Value: 42})
	testWrap(t, int32(42), &wrappers.Int32Value{Value: 42})
}

func TestWrapInt64(t *testing.T) {
	testWrap(t, int(42), &wrappers.Int64Value{Value: 42})
	testWrap(t, int64(42), &wrappers.Int64Value{Value: 42})
}

func TestWrapUInt32(t *testing.T) {
	testWrap(t, uint8(42), &wrappers.UInt32Value{Value: 42})
	testWrap(t, uint16(42), &wrappers.UInt32Value{Value: 42})
	testWrap(t, uint32(42), &wrappers.UInt32Value{Value: 42})
}

func TestWrapUInt64(t *testing.T) {
	testWrap(t, uint(42), &wrappers.UInt64Value{Value: 42})
	testWrap(t, uint64(42), &wrappers.UInt64Value{Value: 42})
}

func TestWrapFloat32(t *testing.T) {
	testWrap(t, float32(42), &wrappers.FloatValue{Value: 42})
}

func TestWrapFloat64(t *testing.T) {
	testWrap(t, float64(42), &wrappers.DoubleValue{Value: 42})
}

func TestWrapString(t *testing.T) {
	testWrap(t, "Hello World!", &wrappers.StringValue{Value: "Hello World!"})
}

func TestWrapBytes(t *testing.T) {
	testWrap(t, []byte("Hello World!"), &wrappers.BytesValue{Value: []byte("Hello World!")})
}

func TestWrapJSONNumber(t *testing.T) {
	testWrap(t, json.Number("1"), &wrappers.DoubleValue{Value: 1.0})
	testWrap(t, json.Number("1.234"), &wrappers.DoubleValue{Value: 1.234})

	v := json.Number("Hello World!")
	t.Logf("wrapping %v", v)
	out, err := Wrap(v)
	if err == nil {
		t.Fatalf("successfully wrapped non-UTC timestamp %s to %v", v, out)
	}
}

func TestWrapTime(t *testing.T) {
	ts := time.Now().UTC()
	tsmsg, _ := ptypes.TimestampProto(ts)
	testWrap(t, ts, tsmsg)

	loc, _ := time.LoadLocation("Europe/Paris")
	tsInParis := ts.In(loc)
	t.Logf("wrapping %v", tsInParis)
	out, err := Wrap(tsInParis)
	if err == nil {
		t.Fatalf("successfully wrapped non-UTC timestamp %s to %v", tsInParis, out)
	}
}

func TestWrapDuration(t *testing.T) {
	d := time.Since(time.Unix(0, 0))
	dmsg := ptypes.DurationProto(d)
	testWrap(t, d, dmsg)
}

// Helper type for testing things not known to the package
type unknown int

func TestWrapUnknown(t *testing.T) {
	var in unknown = 42
	t.Logf("Wrapping %v", in)
	out, err := Wrap(in)
	if err == nil {
		t.Fatalf("successfully wrapped unsupported type %T to %v", in, out)
	}
	t.Log(err.Error())
}

//
// Unwrapping tests
//

func TestUnwrapNil(t *testing.T) {
	testUnwrap(t, nil, nil)
}

func TestUnwrapBool(t *testing.T) {
	testUnwrap(t, true, true)
	testUnwrap(t, false, false)
}

func TestUnwrapInt32(t *testing.T) {
	testUnwrap(t, int8(42), int32(42))
	testUnwrap(t, int16(42), int32(42))
	testUnwrap(t, int32(42), int32(42))
}

func TestUnwrapInt64(t *testing.T) {
	testUnwrap(t, int(42), int64(42))
	testUnwrap(t, int64(42), int64(42))
}

func TestUnwrapUInt32(t *testing.T) {
	testUnwrap(t, uint8(42), uint32(42))
	testUnwrap(t, uint16(42), uint32(42))
	testUnwrap(t, uint32(42), uint32(42))
}

func TestUnwrapUInt64(t *testing.T) {
	testUnwrap(t, uint(42), uint64(42))
	testUnwrap(t, uint64(42), uint64(42))
}

func TestUnwrapFloat32(t *testing.T) {
	testUnwrap(t, float32(42), float32(42))
}

func TestUnwrapFloat64(t *testing.T) {
	testUnwrap(t, float64(42), float64(42))
}

func TestUnwrapString(t *testing.T) {
	testUnwrap(t, "Hello World!", "Hello World!")
}

func TestUnwrapBytes(t *testing.T) {
	testUnwrap(t, []byte("Hello World!"), []byte("Hello World!"))
}

func TestUnwrapTime(t *testing.T) {
	ts := time.Now().UTC()
	testUnwrap(t, ts, ts)
}

func TestUnwrapDuration(t *testing.T) {
	d := time.Since(time.Unix(0, 0))
	testUnwrap(t, d, d)
}

func TestUnwrapUnknown(t *testing.T) {
	in := &any.Any{TypeUrl: "unknown", Value: []byte{}}
	t.Logf("Unwrapping %v", in)
	out, err := Unwrap(in)
	if err == nil {
		t.Fatalf("unwrapped unsupported type %T to %v", in, out)
	}
	t.Log(err.Error())
}

//
// Concurrency tests
//

func TestConcurrency(t *testing.T) {
	// Test for race conditions using `go test -race`
	wg := sync.WaitGroup{}
	wg.Add(8)

	go func() {
		TestWrapNil(t)
		wg.Done()
	}()
	go func() {
		TestWrapBool(t)
		wg.Done()
	}()
	go func() {
		TestWrapTime(t)
		wg.Done()
	}()
	go func() {
		TestWrapString(t)
		wg.Done()
	}()
	go func() {
		TestUnwrapTime(t)
		wg.Done()
	}()
	go func() {
		TestUnwrapDuration(t)
		wg.Done()
	}()
	go func() {
		TestUnwrapString(t)
		wg.Done()
	}()
	go func() {
		TestUnwrapBytes(t)
		wg.Done()
	}()

	wg.Wait()
}
