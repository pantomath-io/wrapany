// Copyright (c) 2017 Charles Francoise.  All rights reserved.
//
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package wrapany

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	durpb "github.com/golang/protobuf/ptypes/duration"
	"github.com/golang/protobuf/ptypes/empty"
	tspb "github.com/golang/protobuf/ptypes/timestamp"
	"github.com/golang/protobuf/ptypes/wrappers"
)

// Wrap wraps a type from the Go standard library in an Any Protobuf message.
func Wrap(in interface{}) (out *any.Any, err error) {

	var val proto.Message
	switch v := in.(type) {
	// nil maps to Empty
	case nil:
		val = &empty.Empty{}
	// builtin types map to their wrappers
	case bool:
		val = &wrappers.BoolValue{Value: v}
	case int8:
		val = &wrappers.Int32Value{Value: int32(v)}
	case int16:
		val = &wrappers.Int32Value{Value: int32(v)}
	case int32:
		val = &wrappers.Int32Value{Value: int32(v)}
	case int:
		val = &wrappers.Int64Value{Value: int64(v)}
	case int64:
		val = &wrappers.Int64Value{Value: int64(v)}
	case uint8:
		val = &wrappers.UInt32Value{Value: uint32(v)}
	case uint16:
		val = &wrappers.UInt32Value{Value: uint32(v)}
	case uint32:
		val = &wrappers.UInt32Value{Value: uint32(v)}
	case uint:
		val = &wrappers.UInt64Value{Value: uint64(v)}
	case uint64:
		val = &wrappers.UInt64Value{Value: uint64(v)}
	case float32:
		val = &wrappers.FloatValue{Value: v}
	case float64:
		val = &wrappers.DoubleValue{Value: v}
	case string:
		val = &wrappers.StringValue{Value: v}
	case []byte:
		val = &wrappers.BytesValue{Value: v}
	case json.Number:
		var v2 float64
		v2, err = v.Float64()
		if err != nil {
			return
		}
		val = &wrappers.DoubleValue{Value: v2}
	// Time and Duration map to their protobuf counterparts
	case time.Time:
		if v.Location() != time.UTC {
			err = fmt.Errorf("protobuf timestamp wrapper only supports UTC timezone")
			return
		}
		val, err = ptypes.TimestampProto(v)
		if err != nil {
			return
		}
	case time.Duration:
		val = ptypes.DurationProto(v)
	default:
		err = fmt.Errorf("unsupported type %T", in)
		return
	}

	out, err = ptypes.MarshalAny(val)
	return
}

// Unwrap unwraps a Protobuf Any message and returns a type from the Go standard
// library.
func Unwrap(in *any.Any) (out interface{}, err error) {

	var val ptypes.DynamicAny
	err = ptypes.UnmarshalAny(in, &val)
	if err != nil {
		return
	}

	switch v := val.Message.(type) {
	// Empty maps to nil
	case *empty.Empty:
		out = nil
	// wrappers map to builtin types
	case *wrappers.BoolValue:
		out = v.GetValue()
	case *wrappers.Int32Value:
		out = v.GetValue()
	case *wrappers.Int64Value:
		out = v.GetValue()
	case *wrappers.UInt32Value:
		out = v.GetValue()
	case *wrappers.UInt64Value:
		out = v.GetValue()
	case *wrappers.FloatValue:
		out = v.GetValue()
	case *wrappers.DoubleValue:
		out = v.GetValue()
	case *wrappers.StringValue:
		out = v.GetValue()
	case *wrappers.BytesValue:
		out = v.GetValue()
	// Timestamp and duration map to their Go counterparts
	case *tspb.Timestamp:
		out, err = ptypes.Timestamp(v)
		if err != nil {
			return
		}
	case *durpb.Duration:
		out, err = ptypes.Duration(v)
		if err != nil {
			return
		}
	default:
		err = fmt.Errorf("unsupported type %s", proto.MessageName(val))
		return
	}

	return
}
